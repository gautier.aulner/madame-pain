export type Product = {
	id: number;
	imgUrl: string;
	title: string;
	description?: string;
	recipe?: string[];
	price?: number;
	allergens?: string[];
	type?: number;
};

export type Supplier = {
	id: number;
	name: string;
	imgUrl: string;
	description: string;
	supply: string[];
	contact: {
		name: string;
		phone: string;
		email: string;
	};
};
