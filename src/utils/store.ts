import { writable } from 'svelte/store';

export const scrollY = writable(0);
export const store_headerHeight = writable(100);
export const store_footerHeight = writable(100);
export const store_screenWidth = writable(0);
export const showMenu = writable(false);
export const showReturn = writable('');
