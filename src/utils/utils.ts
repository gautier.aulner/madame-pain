import { goto } from '$app/navigation';
import { showMenu, showReturn } from './store';

export function routeToPage(route: string) {
	showMenu.update(() => false);
	showReturn.update(() => '');
	goto(route);
}

export const BASE_URL = import.meta.env.VITE_BASE_URL as string;

import type { Product } from './types';

export const productTypes = [
	{ id: 1, name: 'Pain' },
	{ id: 2, name: 'Viennoiserie' },
	{ id: 3, name: 'Pâtisserie' }
];

export const newProductTypes = [
	{ id: 1, name: 'Pain' },
	{ id: 2, name: 'Brioche', description: 'À la coupe, pour un délice sans limite !' },
	{
		id: 3,
		name: 'Croissant',
		description: 'En vente le weekend, pour un réveil tout en douceur...'
	},
	{
		id: 4,
		name: 'Spéciaux',
		description:
			'Chaque vendredi, retrouvez notre petit-épeautre et notre pain de seigle ! <br /> ' +
			'Biscuits, tartes et chaussons suivant récolte et temps disponible.<br />' +
			'Stock limité, n’hésitez à réserver par téléphone, SMS ou message sur nos réseaux sociaux !'
	}
];

export const productList: Product[] = [
	{
		id: 1,
		title: 'Croissant au beurre',
		imgUrl:
			'https://images.unsplash.com/photo-1555507036-ab1f4038808a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1026&q=80',
		description: 'Mhhhh un bon croissant au beurre.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 2
	},
	{
		id: 2,
		title: "L'originale",
		imgUrl:
			'https://images.unsplash.com/photo-1586765501508-cffc1fe200c8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1171&q=80',
		description: "Quoi de meilleure que l'originale.",
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 3,
		title: 'Chignon',
		imgUrl:
			'https://images.unsplash.com/photo-1585803059817-1e58abb814d0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
		description: "De la tête à l'assiette.",
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 2
	},
	{
		id: 4,
		title: 'La miche de Madame',
		imgUrl:
			'https://plus.unsplash.com/premium_photo-1675727579731-56cddf74c9c1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
		description: 'La seule et unique de Madame.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 5,
		title: 'La Jouflu',
		imgUrl:
			'https://images.unsplash.com/photo-1598373182133-52452f7691ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
		description: 'Tendre et épaisse.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 6,
		title: 'La tresse',
		imgUrl:
			'https://images.unsplash.com/photo-1552302094-91d111d7aad0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1174&q=80',
		description: "De l'artisana à l'état pur.",
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 7,
		title: 'Pain au seigle',
		imgUrl:
			'https://images.unsplash.com/photo-1605974322225-98246b2d683b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1740&q=80',
		description: 'Comme uen baguette mais plus foncé.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 8,
		title: 'Petits pains',
		imgUrl:
			'https://images.unsplash.com/photo-1620167792405-af26ded64cb8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
		description: 'Plein de petits pains pour plus de plaisir.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 9,
		title: 'Farineuse',
		imgUrl:
			'https://plus.unsplash.com/premium_photo-1673052767042-d64c1cbd9f84?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1740&q=80',
		description: 'Farine en +, plaisir en +.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 1
	},
	{
		id: 10,
		title: 'Macarons',
		imgUrl:
			'https://images.unsplash.com/photo-1558326567-98ae2405596b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=759&q=80',
		description: 'Macarons de toutes les couleurs et de toutes les saveurs.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf', 'colorant'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 3
	},
	{
		id: 11,
		title: 'Pockets',
		imgUrl:
			'https://images.unsplash.com/photo-1608039783021-6116a558f0c5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=735&q=80',
		description: 'Surprise surprise !',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 3
	},
	{
		id: 12,
		title: 'Muffins',
		imgUrl:
			'https://images.unsplash.com/photo-1607958996333-41aef7caefaa?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1170&q=80',
		description: 'Pour les petits creux ou les grandes faims.',
		recipe: ['farine', 'beurre', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		price: 1.5,
		allergens: ['gluten', 'lait', 'oeuf'],
		type: 3
	}
];

export const newProductList: Product[] = [
	{
		id: 1,
		title: 'L’incontournable baguette (250g)',
		imgUrl: '/product/baguette.png',
		// 'https://images.unsplash.com/photo-1554475659-9fd915c8f156?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1964&q=80',
		type: 1
	},
	{
		id: 2,
		title: 'Graines (noisettes ou noix)',
		imgUrl:
			// 'https://plus.unsplash.com/premium_photo-1672865362974-255c69ebee7c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80',
			'/product/pain-noix.jpg',
		type: 1
	},
	// {
	// 	id: 3,
	// 	title: 'La miche croustillante aux noisettes (500g)',
	// 	imgUrl:
	// 		'https://images.unsplash.com/photo-1567042661848-7161ce446f85?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1965&q=80',
	// 	type: 1
	// },
	{
		id: 4,
		title: 'Miche de Madame - 500g',
		imgUrl: '/product/miche-de-madame.jpg',

		// 'https://images.unsplash.com/photo-1676138937661-63d9fa279445?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1887&q=80',
		type: 1
	},
	{
		id: 5,
		title: 'Pain paysan d’1,3 kg nature et aux graines (Lin/Tournesol)',
		description: 'Pour un apport équilibré en Oméga',
		imgUrl: '/product/paysan.png',
		// 'https://images.unsplash.com/photo-1511629036492-6c07153d3e83?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80',
		type: 1
	},
	{
		id: 6,
		title: 'Nature',
		imgUrl:
			// 'https://images.unsplash.com/photo-1552056413-b8b5eed0170b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1929&q=80',
			'/product/brioche-nature.jpg',
		type: 2
	},
	{
		id: 7,
		title: 'Cannelle',
		imgUrl: '/product/brioche-cannelle.jpg',
		// 'https://images.unsplash.com/photo-1614205570119-2335a1434cca?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1799&q=80',
		type: 2
	},
	{
		id: 8,
		title: 'Nature',
		imgUrl:
			'https://images.unsplash.com/photo-1676723259387-623031241594?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1770&q=80',
		type: 3
	},
	{
		id: 9,
		title: 'Chocolat',
		imgUrl:
			'https://images.unsplash.com/photo-1604639892949-6c7e0b858774?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1897&q=80',
		type: 3
	},
	{
		id: 10,
		title: 'Petit-épeautre',
		imgUrl: '/product/epeautre.jpg',
		// 'https://images.unsplash.com/photo-1604639892949-6c7e0b858774?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1897&q=80',
		type: 4
	},
	{
		id: 10,
		title: 'Pain au seigle',
		imgUrl: '/product/meteil.jpg',
		// 'https://images.unsplash.com/photo-1604639892949-6c7e0b858774?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1897&q=80',
		type: 4
	}
];
