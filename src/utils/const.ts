import { HomeIcon, MapPinIcon, ShoppingCartIcon, TruckIcon } from 'svelte-feather-icons';
import type { Supplier } from './types';

export const smallScreen = 768;
export const mediumScreen = 1024;

export const ITEM_ICON = {
	oeuf: 'icon-park-outline:egg',
	blé: 'game-icons:wheat',
	lait: 'icon-park-outline:milk',
	yaourt: 'icon-park-outline:milk',
	sucre: 'ep:sugar',
	levure: 'mdi:yeast',
	sel: 'tabler:salt',
	crème: 'icon-park-outline:hand-cream',
	fromage: 'icon-park-outline:cheese',
	farine: 'game-icons:flour',
	beurre: 'fluent-emoji-high-contrast:butter',
	gluten: 'mdi:gluten-free',
	grain: 'ph:grains-bold',
	chocolat: 'game-icons:chocolate-bar',
	fruit: 'healthicons:fruits'
};

export let navItems = [
	{
		title: ' Accueil',
		link: '/',
		icon: HomeIcon,
		iconParam: { strokeWidth: 1 },
		active: false
	},
	{
		title: 'Produits',
		link: '/produit',
		icon: ShoppingCartIcon,
		iconParam: { strokeWidth: 1 },
		active: false
	},
	// {
	// 	title: 'Reseaux',
	// 	link: '/reseau',
	// 	icon: UserIcon,
	// 	iconParam: { strokeWidth: 1 },
	// 	active: false
	// },
	{
		title: 'Fournisseurs',
		link: '/fournisseurs',
		icon: TruckIcon,
		iconParam: { strokeWidth: 1 },
		active: false
	},
	{
		title: 'Me trouver',
		link: '/metrouver',
		icon: MapPinIcon,
		iconParam: { strokeWidth: 1 },
		active: false
	}
];

export const suppliers: Supplier[] = [
	{
		id: 1,
		name: 'Moulin de la Chaume',
		imgUrl:
			'https://www.moulindelachaume.fr/wp-content/uploads/2019/03/logo-moulin-de-la-chaume.png',
		description:
			'Moulin de la Chaume est un moulin à eau situé à Saint-Georges-de-Montaigu, e Moselle. Il est l’un des derniers moulins à eau en activité en France.',
		supply: ['farine', 'sucre', 'sel', 'levure', 'lait', 'oeuf'],
		contact: {
			name: 'Moulin de la Chaume',
			phone: '02 51 52 95 95',
			email: 'moulinChaume@gmail.com'
		}
	},
	{
		id: 2,
		name: 'La Laiterie de Montaigu',
		imgUrl:
			'https://www.laiteriedemontaigu.fr/wp-content/uploads/2019/03/logo-laiterie-de-montaigu.png',
		description:
			'La Laiterie de Montaigu est une entreprise française de transformation laitière, basée à Montaigu, en Moselle. Elle est spécialisée dans la fabrication de produits laitiers frais.',
		supply: ['lait', 'beurre', 'crème', 'yaourt', 'fromage'],
		contact: {
			name: 'La Laiterie de Montaigu',
			phone: '02 51 46 00 00',
			email: 'laiterieMontaigu'
		}
	},
	{
		id: 3,
		name: 'La Ferme de la Gournerie',
		imgUrl:
			'https://www.lafermedelagournerie.fr/wp-content/uploads/2019/03/logo-ferme-de-la-gournerie.png',
		description:
			'La Ferme de la Gournerie est une entreprise française de transformation laitière, basée à Montaigu, en Moselle. Elle est spécialisée dans la fabrication de produits laitiers frais.',
		supply: ['blé', 'farine', 'oeuf'],
		contact: {
			name: 'La Ferme de la Gournerie',
			phone: '02 51 46 00 00',
			email: 'fermeGournerie@gmail.com'
		}
	}
];

// export const ingredients = [
// 	{
// 		name: 'farine',
// 		icon: ITEM_ICON.farine,
// 		description:
// 			'Heureuse de vous présenter Philippe, la graine du projet Madame Pain... et c’est le cas de le dire ! 120 hectares de terrain en forme de demi-lune sur les hauteurs desvallées de l’Argonne, cultivés dans une collaboration fine avec la nature. Philippe Fourmet est Paysan-Meunier en agriculture Demeter à Récicourt qui nous fournit chaque mois en farine de blé (T65 et T80sur base de blés anciens), farine de seigle (T110) et de petit-épeautre(T80).'
// 	},
// 	{
// 		name: 'sel',
// 		icon: ITEM_ICON.sel,
// 		description:
// 			'L’industrie agroalimentaire, pour des questions d’esthétisme, de praticité, et de forme, a choisi de raffiner le sel marin. Ce processus utilise des adjuvants, et des composés fluorés/iodés ainsi que l’ajout d’anti-agglomérants et de l’iodure de potassium pendant la phase de séchage ce qui élimine tous les nutriments du sel (minéraux et oligo-éléments). A la fin du processus de raffinage, le produit obtenu est privé de tousses nutriments d’origine, il est « vide ». Notre organisme a plus de mal à l’assimiler, le transformer, et l’intégrer. Avant l’ouverture de Madame Pain, nous avons donc enfiler notre casque et sommes descendus à 160m sous terre pour découvrir la Mine de Varangéville, proche de Nancy ! En compagnie de Denis et de son équipe, nous avons découvert LA DERNIÈRE MINE DE SEL DE FRANCE où les derniers mineurs œuvrent chaque jour dans 300km de galerie pour nous proposer du sel de route (évitons de le mettre dans le pain celui-là) et du gros sel utilisable en Agriculture Biologique carbrut et non transformé !'
// 	},
//     {
//         name: 'Les produits laitiers et oeufs',
//         icon: ITEM_ICON.lait,
//         description: 'Dans les métiers de bouche, il peut être difficile de se fournir en local... question de quantité... et notamment pour le beurre que nous avons choisi biologique et tout de même Français malgré–vous vous en doutez – un prix franchement plus élevé. Le beurre vient alors des vaches normandes (marque Isigny Sainte-Mère) et le lait des vaches Lorraines (ferme des Grands Près)... Idempour les œufs (bien entendu, ce sont des poules et non des vaches...).'
//     }
// ];
