export type Horaire = {
	id?: string;
	jour: string;
	horaireFrom: string;
	horaireTo: string;
};

export type Marche = {
	id?: string;
	jour: string;
	city: string;
	dayTime?: string;
};

export type Place = {
	id?: string;
	text: string;
};
