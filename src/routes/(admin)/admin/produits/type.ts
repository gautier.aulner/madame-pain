export type Category = {
	id: CategoryId;
	name: string;
	description: string;
	order: number;
};

export type CategoryInput = {
	id?: string;
	name: string;
	description: string;
};

export type ProductFB = {
	id: string;
	name: string;
	description?: string;
	price?: number;
	imgUrl?: string;
	imgName?: string;
	categoryId: CategoryId;
};

export type ProductInput = {
	name: string;
	categoryId: CategoryId;
	image: File;
};

export type CategoryId = string;
