/** @type {import('tailwindcss').Config} */
import colors from 'tailwindcss/colors';
// const colors = require('tailwindcss/colors');

export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		extend: {},
		colors: {
			// Now we build the full color palette, using all colors available
			// as shown at this link: https://tailwindcss.com/docs/customizing-colors#color-palette-reference
			transparent: 'transparent',
			current: 'currentColor',
			black: '#000',
			white: '#fff',
			slate: colors.slate,
			gray: colors.gray,
			neutral: colors.neutral,
			stone: colors.stone,
			red: colors.red,
			orange: colors.orange,
			amber: colors.amber,
			yellow: colors.yellow,
			lime: colors.lime,
			green: colors.green,
			emerald: colors.emerald,
			teal: colors.teal,
			cyan: colors.cyan,
			sky: colors.sky,
			blue: colors.blue,
			indigo: colors.indigo,
			violet: colors.violet,
			purple: colors.purple,
			fuchsia: colors.fuchsia,
			pink: colors.pink,
			rose: colors.rose,
			primary: {
				100: '#fff5eb',
				200: '#ffecd7',
				300: '#ffe2c3',
				400: '#ffd9af',
				500: '#ffcf9b',
				600: '#cca67c',
				700: '#997c5d',
				800: '#66533e',
				900: '#33291f'
			},
			// primary: {
			// 	100: '#efd8d7',
			// 	200: '#deb1b0',
			// 	300: '#ce8988',
			// 	400: '#bd6261',
			// 	500: '#ad3b39',
			// 	600: '#8a2f2e',
			// 	700: '#682322',
			// 	800: '#451817',
			// 	900: '#230c0b'
			// },
			// primary: {
			// 	100: '#fbf9f7',
			// 	200: '#f7f3ef',
			// 	300: '#f4ede6',
			// 	400: '#f0e7de',
			// 	500: '#ece1d6',
			// 	600: '#bdb4ab',
			// 	700: '#8e8780',
			// 	800: '#5e5a56',
			// 	900: '#2f2d2b'
			// },
			gris: {
				1: '#111111',
				2: '#222222',
				3: '#333333'
			},
			noir: {
				100: '#d3d3d3',
				200: '#a7a7a7',
				300: '#7a7a7a',
				400: '#4e4e4e',
				500: '#222222',
				600: '#1b1b1b',
				700: '#141414',
				800: '#0e0e0e',
				900: '#070707'
			}
		}
	},
	plugins: []
};
